//Obtendo a conexão com o banco de dados
const connect = require("../db/connect");

module.exports = class clienteController {
  static async postCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body;

    //verificando se o atrivuto chave é diferente de 0 (zero)
    if (telefone !== 0) {
      const query = `insert into cliente(telefone,nome,cpf,logradouro,numero, complemento,bairro,cidade,estado,cep,referencia)
         values('${telefone}','${nome}','${cpf}','${logradouro}','${numero}','${complemento}','${bairro}','${cidade}','${estado}','${cep}','${referencia}'
        )`;

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Usuario não cadastrado no banco" });
            return;
          }
          console.log("Inserido no banco");
          res.status(201).json({ message: "Usuario criado com sucesso" });
        });
      } catch (error) {
        console.error("Erro ao executar o insert -", error);
        res.status(500).json({ error: "Erro interno do servidor" });
      } //fim catch
    } //fim da query
    else {
      res.status(400).json({ message: "O Telefone é obrigatório" });
    }
  } //fim function

  static async getAllCliente(req, res) {
    const query = `select * from cliente`;
    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuarios nao encontrados no banco!!" });
          return;
        }
        let clientes = data;
        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ clientes });
      }); //fim da query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", -error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
    } //fim catch
  } //fim function

  static async getAllClienteParams(req, res) {
    //metodo para selecionar clientes via parametros especificos

    //Extrair parametros da consulta da URL
    const { filter, ordering, order } = req.query;

    //Construir a consulta SQL base
    let query = `select * from cliente`;

    //Adicione a clausula where, quando houver
    if (filter) {
      //query = query + filter; - está incorreto porque a sintaxe deve ser do mysql
      //query = query + ` where ${filter}`; logo embaixo está a forma resumida dessa linha
      query += ` where ${filter} `;
    } //fim if filter

    //http://localhost:5000/api/pizzaria/getClienteParams/?filter=cep=%221100000%22
    

    //Adicionar a cláusula order by, quando houver 
    if(ordering){
      query += ` order by ${ordering} `;

    //Adicionar a ordem do order by (asc ou desc)
    if(order){
      query += ` ${order} `;
    }//fim if order
  }

  //http://localhost:5000/api/pizzaria/getClienteParams/?filter=cep=%2211000000%22&ordering=nome&order=desc
    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuarios nao encontrados no banco!!" });
          return;
        }
        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ result });
      }); //fim da query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", -error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
    } //fim catch
  }


}; //fim class
