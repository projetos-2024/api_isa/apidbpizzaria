const router = require("express").Router();
const dbController = require("../controller/dbController");
const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaController");


//rota para consultas de tabelas do banco
router.get("/tables", dbController.getTables);

//rota para cadastro de clientes
router.post("/postCliente", clienteController.postCliente);
router.get("/getCliente", clienteController.getAllCliente);
router.get("/getClienteParams", clienteController.getAllClienteParams);

//rota para listar pedidos das pizzas
router.get("/orderList", pizzariaController.orderList);

//rota para listar pedidos das pizzas com Join
router.get("/orderListJoin", pizzariaController.orderListJoin);

//rota para listar pedidos das pizzas com InnerJoin
router.get("/orderListInnerJoin", pizzariaController.orderListInnerJoin);


module.exports = router;
